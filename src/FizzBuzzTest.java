import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FizzBuzzTest {
    private Game game;

    @BeforeEach
    void setUp() {
        game = new Game();
    }

    @Test
    void test_say_1_for_1() {
        int actual = game.say(1);

        assertEquals(1, actual);
    }

    @Test
    void test_say_2_for_2() {
        int actual = game.say(2);

        assertEquals(2, actual);
    }
}